package listener;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import data.ReadCounter;
import data.SendReads;
import data.Sender;
import util.CommonUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Created by ravi on 27/7/17.
 */
@WebServlet(name = "StartReading")
public class StartReading extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//GpioPinDigitalInput myButton=null;
    //GpioController gpio= null;
	CommonUtil util;
	ReadCounter rc;
	SendReads sc;

    @Override
    public void init() throws ServletException{
    	
        super.init();
       //initGpioConfig();
        System.out.println("\n\n\n     listenGpio 11111111 init called");
        rc=ReadCounter.getInstance();
        //sc=SendReads.getInstance();
        //sc.init();
        Sender s=new Sender();
        s.start();

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	handleReads();
    	
    }
	
    
    private void handleReads() {
    	
    	System.out.println("\n\n\n     listenGpio handleReads called");
    	rc.incrementCount();
    	System.out.println("\n\n\n     count is>>"+rc.getReadCount());
    	
    	
    }
    
 /*   private void listen(){

    }*/
  /*  private void initGpioConfig(){

        gpio= GpioFactory.getInstance();
        myButton=gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_DOWN);
        myButton.setShutdownOptions(true);
        myButton.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());

            }
        });
    }*/
}
