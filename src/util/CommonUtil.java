package util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import com.emeter.asset.meter.MeterReadings;
import com.fasterxml.jackson.annotation.JsonInclude;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtil {
	
	private static CommonUtil _instance=new CommonUtil();
	
	public static CommonUtil getInstance() {
		return _instance;
	}
	
    private static ObjectMapper mapper = new ObjectMapper()

            .setSerializationInclusion(JsonInclude.Include.NON_NULL);
	

	public HttpRequestBase createHttpMethod(String url, String method, String jsonInput,String contentType,String charset) throws UnsupportedEncodingException {
		
		
		if ("GET".equalsIgnoreCase(method)) {
			return new HttpGet(url);
		} else if ("POST".equalsIgnoreCase(method)) {
			HttpPost pm = new HttpPost(url);
			pm.setHeader("Content-Type", contentType);
			System.out.println("\n\n>>>>>>>>>>>>>>>>>jsonInput>"+jsonInput);
			StringEntity entity = new StringEntity(jsonInput, ContentType.create(contentType , charset));
			pm.setEntity(entity);
			return pm;
		} else if ("PUT".equalsIgnoreCase(method)) {
			HttpPut pm = new HttpPut(url);
			pm.setHeader("Content-Type", contentType);
			StringEntity entity = new StringEntity(jsonInput, ContentType.create(contentType , charset));
			pm.setEntity(entity);
			return pm;
		} else if ("DELETE".equalsIgnoreCase(method)) {
			return new HttpDelete(url);
		} else {
			throw new RuntimeException("Unsupported method - "+method);
		}
	}
	
	   public static <T> String toJSON(final Object t, final Class<T> clazz) {

	        try {

	            return mapper.writeValueAsString(t);

	        } catch (JsonProcessingException e) {

	            e.printStackTrace();
	            return "";
	        }

	    }
	   
	   public static void main(String[] args) {
	    /*	MeterReadings mr = new MeterReadings();
	    	//MeterAttribute mra = new MeterAttribute();
	    	mr.setName("test");
	    	//mr.setList(Arrays.asList(mra));
			System.out.println(toJSON(mr,MeterReadings.class));*/
		   
		   CommonUtil u=new CommonUtil();
			System.out.println(getCurrentTime());
			
		}
	   
	   public static String getCurrentTime() {
		   
		   SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		   Calendar instance = Calendar.getInstance();
		   instance.set(Calendar.SECOND, 00);
		   
		   return sd.format(instance.getTime());
	   }
}
