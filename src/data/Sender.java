package data;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Sender  {

	
	public void start() {
		ScheduledExecutorService sec=Executors.newScheduledThreadPool(5);
		
		Runnable sr=new SendReads();
		sec.scheduleAtFixedRate(sr, 60, 60, TimeUnit.SECONDS);
		
		System.out.println("<<<<<<<<<<Executor started>>>>>>>>>>>>>>");

	}

}
