package data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.emeter.asset.meter.MeterReadings;

import data.ReadCounter;
import util.CommonUtil;

public class SendReads implements Runnable {

	//private static final SendReads _instance=new SendReads();
	
	private String contentType = "application/json";
	private String charset = "utf-8";
	private static  CloseableHttpClient client=null;
	private int connectionTimeOut = 10000;
	private int socketTimeOut = 10000;	
	private int connectionRequestTimeOut = 10000;
	CommonUtil util;
	MeterReadings mt;
	ReadCounter rc;

	/*private SendReads() {
		//throw new NullPointerException();
	}*/
	
	/*public static SendReads getInstance() {
		return _instance;
	}*/
	
	public SendReads() {

    	util=CommonUtil.getInstance();
    	rc=ReadCounter.getInstance();
    	
   	 RequestConfig defaultRequestConfig = RequestConfig.custom()
		        .setConnectTimeout(connectionTimeOut)
		        .setSocketTimeout(socketTimeOut)
		        .setConnectionRequestTimeout(connectionRequestTimeOut)
		        .build();
	 client = HttpClientBuilder
	            .create()
	            .setDefaultRequestConfig(defaultRequestConfig)
	            .build();
    	
        System.out.println("\n\n\n     SendReads init called");

        //sendReads();
	}
	
	/*public String sendReads() {
		
		ScheduledExecutorService sec=Executors.newScheduledThreadPool(5);
		
		sec.scheduleWithFixedDelay(_instance, 10, 10, TimeUnit.SECONDS);
		
		return "success";
		
	}*/

	@Override
	public void run() {
		System.out.println("<<<<<<<<<<<run called>>>>>>>>>>>>>");
		ReadCounter rc=ReadCounter.getInstance();
		
		System.out.println("<<<<<<<<<<<run called>>>>>>>>>>>>>"+rc);
		int reads=rc.getReadCount();
		System.out.println("<<<<<<<<<<<run called>>>>>>>>>>>>>"+reads);
		rc.resetReadCount();
		System.out.println("<<<<<<<<<<<run called>>>>>>>>333>>>>>"+reads);
		
		
		HttpRequestBase httpMethod=null;
    	try {
    	   	/*MeterReadings mr = new MeterReadings();
        	mr.setName("test");
        	mr.setValue("testVal");
        	//mr.setList(Arrays.asList(mra));
    		System.out.println(util.toJSON(mr,MeterReadings.class));
    		*/
    		mt=new MeterReadings();
    		mt.setName("Test Meter");
    		mt.setId("MTR-ELEC-10001");
    		mt.setTicks(reads);
    		mt.setRecordTime(CommonUtil.getCurrentTime());
    		
    		System.out.println("<<<<<<<<<<<run called>>>>>>>>>>>>>"+mt);
    		String jsonInput=CommonUtil.toJSON(mt, MeterReadings.class);
    		System.out.println("\n\n\n\n     jsonInput>>>"+jsonInput);
    		
    		System.out.println("\n\n\n\n     client>>>"+client);
    		httpMethod = util.createHttpMethod("http://localhost:8090/em-smartgateway/smartconversion/gateway/meterreads", "POST", jsonInput,contentType,charset);
    		System.out.println("\n\n\n\n     httpMethod>>>"+httpMethod);
    		CloseableHttpResponse responseObj=client.execute(httpMethod);

    		System.out.println("respons obj>>>>>>>>."+responseObj);

    		if(responseObj.getEntity() != null){
    			StringBuffer buffer = new StringBuffer();
    			BufferedReader reader = new BufferedReader(new InputStreamReader(responseObj.getEntity().getContent()));
    			String dataLine = null;
    			while((dataLine = reader.readLine()) != null){
    				buffer.append(dataLine);
    			}
    			String responseTxt =  buffer.toString();
    			System.out.println("\n\n\n     listenGpio doGet called responseTxt>>>>>>>>"+responseTxt);
    			//return response;
    		}else{
    			System.out.println("\n\n\n     listenGpio doGet called responseTxt is null >>>>>>>>");    		 }
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	finally{
    		if(httpMethod != null){
    			httpMethod.releaseConnection();
    		}
		
    	}
		
	}
}
