package data;


public class ReadCounter {

	private static final ReadCounter _instance=new ReadCounter();
	
	private int counter=0;
	
	private ReadCounter() {
	}
	
	public static ReadCounter getInstance() {
		return _instance;
	}
	
	public void incrementCount() {
		++_instance.counter;
	}
	
	public int getReadCount() {
		return _instance.counter;
	}
	
	public void resetReadCount() {
		_instance.counter=0;
	}
}
