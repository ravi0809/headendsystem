<%--
  Created by IntelliJ IDEA.
  User: ravi
  Date: 4/6/17
  Time: 2:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui.css"/>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>
Control your Appliances
</h1>


<form name="ajaxForm" method="post">
    <table>
        <tr>
            <td>
                Bulb:
            </td>
            <td>
                <input type="checkbox" name="bulb" id="bulb" onclick="toggleButton()">
                <select name="intensity" id="intensity" onclick="changeIntensity()">
                    <option value="0">off</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </td>
        </tr>

    </table>

</form>


</form>
<script>
function toggleButton(){
    var buttonState="off";
    if($("#bulb").prop("checked")){
        buttonState="on";
    }
    $.ajax({
       url:"controllerServlet?fromAjax=yes&buttonState="+buttonState
    });
}

function changeIntensity(){

    var intensity=document.ajaxForm.intensity.value;
    alert(intensity);

    $.ajax({
        url:"controllerServlet?fromAjax=yes&intensity="+intensity
    });

}
</script>
</body>
</html>
